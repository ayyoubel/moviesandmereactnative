// Components/Favorites.js

import React from 'react'
import { StyleSheet, Text, FlatList, View } from 'react-native'
import { connect } from 'react-redux'
import FilmItem from './FilmItem';

class Favorites extends React.Component {

    _displayDetailForFilm = (film) => {
        console.log('click');
        this.props.navigation.navigate('filmDetail', { film });
    }

    render() {
        console.log("**************************")
        console.log()
        return (
            <View style={styles.main_container}>
                <FlatList
                    data={this.props.favoritesFilm}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({ item }) => <FilmItem film={item} filmDetail={this._displayDetailForFilm} />}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({})

const mapStateToProps = state => {
    return {
        favoritesFilm: state.favoritesFilm
    }
}

export default connect(mapStateToProps)(Favorites)