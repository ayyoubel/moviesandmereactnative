const API_TOKEN = "67d6d74a069765c79ec048abc2507404";

export function getFilmsFromApiWithSearchedText(text, page) {
    const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + text + "&page=" + page
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.error(error))
}

export function getFilmById(idFilm) {
    const url = 'https://api.themoviedb.org/3/movie/' + idFilm + '?api_key=' + API_TOKEN + '&language=fr';
    return fetch(url).then((response) => response.json()).catch((error) =>console.log(error));
}

export function getImageFromApi(name) {
    return 'https://image.tmdb.org/t/p/w300' + name
}